import { Injectable } from '@angular/core';
import { SetoresType } from '../types/setores.setor';

@Injectable({
  providedIn: 'root'
})
export class SetoresService {

  readonly $setores: SetoresType[] = [
    {
      key: '1',
      name: 'SSA'
    },
    {
      key: '2',
      name: 'SSJ'
    }
  ];

  constructor() { }


  list(): SetoresType[] {
    return this.$setores;
  }

}
