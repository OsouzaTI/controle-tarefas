import { Routes } from '@angular/router';
import { SetoresComponent } from './index/setores.component';
import { AddSetorComponent } from './add-setor/add-setor.component';

export const SETORES_ROUTES: Routes = [
  { path: '', component: SetoresComponent },
  { path: 'add', component: AddSetorComponent },
];
