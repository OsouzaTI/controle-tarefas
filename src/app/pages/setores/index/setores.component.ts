import { Component, OnInit } from '@angular/core';
import { SetoresService } from '../../../services/setores.service';
import { SetoresType } from '../../../types/setores.setor';


@Component({
  selector: 'app-setores',
  templateUrl: './setores.component.html',
  styleUrl: './setores.component.scss'
})
export class SetoresComponent implements OnInit {


  constructor(private setoresService: SetoresService) {

  }

  ngOnInit(): void {
  }

  list() {
    return this.setoresService.list()
  }

}
