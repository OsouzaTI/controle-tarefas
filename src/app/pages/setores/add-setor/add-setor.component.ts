import { Component } from '@angular/core';
import { FormControl, FormGroup, NonNullableFormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-setor',
  templateUrl: './add-setor.component.html',
  styleUrl: './add-setor.component.scss',
})
export class AddSetorComponent {

  validateForm: FormGroup<{
    setor: FormControl<string>;
  }> = this.fb.group({
    setor: ['', [Validators.required]],
  });

  submitForm(): void {
    console.log('submit', this.validateForm.value);
  }

  constructor(private fb: NonNullableFormBuilder) {}

}
