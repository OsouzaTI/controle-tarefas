import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSetorComponent } from './add-setor/add-setor.component';
import { SetoresComponent } from './index/setores.component';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { RouterLink, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SETORES_ROUTES } from './setores.routes';


@NgModule({
  declarations: [
    SetoresComponent,
    AddSetorComponent
  ],
  imports: [
    CommonModule,
    NzFormModule,
    NzTableModule,
    NzDividerModule,
    NzButtonModule,
    ReactiveFormsModule,
    RouterLink,
    RouterModule.forChild(SETORES_ROUTES)
  ],
  exports: [
    SetoresComponent,
    AddSetorComponent
  ]
})
export class SetoresModule { }
